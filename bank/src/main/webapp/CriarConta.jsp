<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="domain.ContaBanco, persistence.*, java.sql.SQLException" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastro bem sucedido</title>
</head>
<body>
	<%
	String name = request.getParameter("nome").toUpperCase();
	String type = request.getParameter("tipo").toUpperCase();
	ContaBanco.abrirConta(name, type);
	%>
	<a href="index.html">Voltar</a>
</body>
</html>